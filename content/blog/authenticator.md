+++
title = "Authenticator"
author = "petrisch"
date = 2020-02-01
+++

## Authenticator App

Some rambling with the todays status of the Authenticator App from here:
[flatpak](https://flathub.org/apps/details/com.belmoussaoui.Authenticator)

Installation with flatpak was ok after reaching out to the devs, thx!

Now starting no problem:  

![auth1](../1_PP_Authenticator.jpg)

Hitting the `+` sing for a new account, works:  

![auth2](../2_PP_Authenticator.jpg)

Pressing `u` and selecting Unity work:  

![auth3](../3_PP_Authenticator.jpg)

Adding the Account and Token works:  

![auth4](../4_PP_Authenticator.jpg)

Beeing stupid about entering the wrong Token, breaks the App:  

![auth5](../5_PP_Authenticator.jpg)


Using the right Token establishes the Account:  
![auth6](../6_PP_Authenticator.jpg)

But using the Account to log in again crashes the App again.  
Now we have to reboot the Phone again and try later, starting the App from the Terminal first to see whats going on.

