+++
title = "Cherrytree"
author = "petrisch"
date = 2020-07-05
+++

# Cherrytree on a mobile device

Cherrytree is one of the apps I don't know yet for a long time, but its really an interesting charmfull one.
And it would be very nice to have that one abroad on a phone. Since there is no cherrytree on Android or iPhone,
that would be one more thing, that I could do on this phone that I can't on any other.  

I think the Use Case for Cherrytree on mobile is to mainly read notes, and not to write long essays.  
But I think thats exactly what makes it quite valuable to have!    

And with SSH into the phone and writing these lines in ``nano`` directly, if thats not something?  

## My journey that I had with cherrytree on the pinephone so far  
How it looks like on a desktop computer.  

![cherrytree desktop](../chery2_desktop.png)

Now we want to get it running on the pinephone

Get it from flatpak and run it => easy  
``sudo apt install cherrytree``

But it looks like this:

![cherrytree full](../chery1.png)

In the preview we see where the rest of it goes:

![cherrytree phosh preview](../chery.png)

The suggestion on the mobian wiki says it can be made with scale-to-fit.

``scale-to-fit cherrytree on``

Didn't seem to work, even scaled like this:

``WAYLAND_DEBUGG=client cherrytree``

There was no app ID output telling me what to use.  
At this moment this would may better be a blog than a flat site, but let's continue with an update as of  
**07th of december 2020:**

Really the problem was, that cherrytree was heavily dependent on python2 and was no way running on WAYLAND at all, so scale-to-fit did just nothing.  
So not really knowing that, I opend an issue on [Github](https://github.com/giuspen/cherrytree/issues/1024) which got shortly after answered by the maintainer, thank you!  
Now with cherrytree 0.99.24 being on flathub this changed to the better.

Following the instructions on [linmob](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html#manual-scaling-in-phosh)

Installing it from flathub  
``flatpak install com.giuspen.cherrytree``

getting the app ID with   
``./get-app-id.sh "flatpak run com.giuspen.cherrytree"``

Gave this surprising result  
``[ 86764.270]  -> xdg_toplevel@28.set_app_id("cherrytree")``

Then:  
``scale-to-fit cherrytree on``

And the result is:  
![Cherrytree full2](../screenshot_cherrytree.png)

Now I "can" use it! Shure the menu and knots are tiny and hard to hit, but still.  
**Libhandy** would help make it even better, but thats probably not that easy, I don't know.  
There are probably other, easier approaches to make the scaling better, but all of them are out of my scope.  


The journey will continue...


