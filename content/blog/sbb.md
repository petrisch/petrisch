+++
title = "SBB"
author = "petrisch"
date = 2020-01-10
+++

# Innoffizielle SBB Fahrplan App

Aufs neue Jahr ist mir auf der [linmob](https://linmob.net/) Liste eine App aufgefallen, die ich hier gerne vorstellen möchte.
Das Programm zielt auf mobile Linux Geräte ab, welche in letzter Zeit mit dem Pinephone und Librem 5 Aufschwung erleben.

Die App heisst ganz einfach "SBB" und ist auf Github zu finden.
[io.chefe.sbb](https://github.com/chefe/sbb)

<img src="../sbb.png" alt="Homescreen" style="width: 190px;"/> 

Im Wesentlichen greift sie die Daten von [transport.opendata.ch](https://transport.opendata.ch/) ab, womit man sich die Verbindung sehr vieler öffentlicher Verkehrsmittel in der Schweiz anzeigen lassen kann.

Die App beschränkt sich auf das Nötigste, was ich persönlich sehr angenehm finde.

<img src="../sbb1.png" alt="Zeitauswahl" style="width: 190px;"/> 
<img src="../sbb2.png" alt="Fahrplananzeige" style="width: 190px;"/> 


Der Dialog zum Einstellen der Zeit finde ich sehr gelungen, ein Herzstück der App.

Die App ist in GTK3 und Rust geschrieben. Da ich zu Weihnachten das Rust Buch geschenkt bekommen habe, habe ich auch kurz in den Code geschaut und konnte einige Konzepte wiederentdecken.

Einen kleinen Beitrag konnte ich auch schon leisten. Die Favoriten wurden per Kommatrennung gespeichert, was sich nicht mit allen Haltestellennamen verträgt.

Es gibt sicher noch einiges an "fehlender" Funktionalität und eine Konkurrenz zur offiziellen SBB Android App ist es natürlich bei weitem nicht. Speziel eine Funktion um sichere Zahlungen zu tätigen, wird es in diesem Ökosystem wohl noch eine Weile nicht geben. Trotzdem eine schöne App und eine angenehme Überraschung aufs neue Jahr.
