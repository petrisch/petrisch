+++
title = "Visit to China"
author = "petrisch"
date = 2021-01-07
+++

# Visit to China

I got a taste of professional training with my 3 visits to china. It wasn't elite players that took my attention. Theyr training isn't much different to the elite players from germany or france. Even though on a higher level and with more background of course.  

I was in primary school, playing with 8-16 years old boys and girls. It was impressive, how determined they were. Not only did they train up to 5h a day while still having school. Before the training started in the morning, you already saw little Po and Ming doing ghost exercise in a almost dark hall. Without coach or ball, just for themselfs and with a lot of fun doing it.  
Championship material one would say.  

I saw light and dark in the chinese systeme. And as always, no need to glorify or overcriticize here. What I mostly saw were human trying theyr best.


![Pausenplatz](../china_pausenplatz.png)

