+++
title = "Pinephone"
author = "petrisch"
date = 2020-07-05
+++

# Pinephone Pictures

This Website is dedicated to my new Pinephone.
Lets see how we can explore the world with it.

So far working on [mobian](https://mobian-project.org/) and yes everything they say is true.
- I can make pictures
- I can get Emails
- I can chat trough fractal/matrix
- I have mastodon which is kind of twitter but better
- I can browse the web
- I can make phone calls
- I can send and receive SMS text messages

Recent News per Jan. 2021:
Its not a daily driver yet, but many improvements have been added
- Battery Live is much better with suspend and runtime improvements. It lasts more than a day now. I don't get any calls though, so I don't know how it wakes up.
- Phone calls have good quality with wired headphones and the internal speaker
- Lollypop works nice with my Jabra Bluetooth headphones. Some edges here and there, but really great already
- My personal favorite: I have added my phone to my ansible update playbook. So to update from file server to tv-box and phone I just use ./update!!

Goals for 2021:
- Get rid of Whatsapp? Maybe a big approach but hey.
- Use my primary SIM for the Phone, so we will see how it works in daily practise.
- Find a fun game I can play on it which enhances fun with using.


Everyone needs a licence right? I don't really know how to do this properly, but I like to have everything under
[Creative Commons](https://de.wikipedia.org/wiki/Creative_Commons)
