+++
title = "Camera"
author = "petrisch"
date = 2020-07-05
+++

# Pinephone Pictures

On this side you can see various pictures taken with the pinephone.  
I am a terrible photographer with absoulutely no knowledge about it.  
Most photos are taken as is, and no longer edited.  

Just recently I started twiddling with [Darktable](https://www.darktable.org/)  

## Librem 5 Spring
[Librem 5 Spring](../librem5_first)

## Butterfly from the papiliorama
[Papiliorama](../papiliorama)

## Pictures from a trip to the tessin
[Tessin](../Tessin)

## Megapixels 1.3 with pictures from around Bern
[Bern](../Bern)

## Pictures from holidays in Bretagne France
[Bretagne](../bretagne)

## Megapixels
[Megapixel Shot of the Alps](../megapixels)

## The Start with Camera App and Pinehole
[Pinehole](../image_pinehole)  
[Pinephone vs. Nokia 6.0](../image_comparison)
