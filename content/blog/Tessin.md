+++
title = "Tessin 2021"
author = "petrisch"
date = 2021-10-09
+++

# Last summer sunshine in the Swiss Tessin

Lake "Lago Maggiore" seen from the "Monte verità"  

![Monte_verita](../Tessin_Monte_Verita.jpg)

A playmobile toy seen on a balcony surrounded by a old village route.  

![Playmobile](../Tessin_Playmobil.jpg)

The village of "Porto Ronco" from the boat.  

![Porto Ronco](../Tessin_Porto_Ronco.jpg)

The view from our balcony at 4 different times of the day.  
I tried to be fancy here by just cutting the edges and laying 4 pictures together.  
Not yet shure if I like it :-)  

![Apartment view](../Tessin_apartment.jpg)

The landscape to the mountains of the valais and italy from the "Nufenenpass".  
Yet another naive approach to make a landscape picture by just overlaying two pictures, taken at the same spot.  

![Nufenen Panorama](../Tessin_nufenen_panorama.jpg)
