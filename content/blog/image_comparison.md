+++
title = "Picture comparison PP and Nokia"
author = "petrisch"
date = 2020-07-05
+++

# Pinephone Pictures

Pictures taken from the pinephone gnome-camera app using mobian on July 12th 2020.
The pictures on the left are from my Nokia 6. This is not the latest and best, but 
actually I never thought I would need any better.

A few words:

- I have very little knowledge about photografic stuff at all
- I just used the defaults on both systems
- I am aware that this is not a fair comparison and it isn't intendet to be one.
- I just shows what you get as a noob out of the box as of today.
- Let's see where this goes from here on.

|| Pinephone  | Nokia         |
| ----------- | ----------- |
| ![pine1](../pine1.jpg) | ![nokia1](../nokia1.jpg) |
| ![pine2](../pine2.jpg) | ![nokia2](../nokia2.jpg) |
| ![pine3](../pine3.jpg) | ![nokia3](../nokia3.jpg) |
| ![pine4](../pine4.jpg) | ![nokia4](../nokia4.jpg) |
| ![pine5](../pine5.jpg) | ![nokia5](../nokia5.jpg) |
| ![pine6](../pine6.jpg) | ![nokia6](../nokia6.jpg) |
| ![pine7](../pine7.jpg) | ![nokia7](../nokia7.jpg) |
| ![pine8](../pine8.jpg) | ![nokia8](../nokia8.jpg) |
| ![pine9](../pine9.jpg) | ![nokia9](../nokia9.jpg) |
| ![pine10](../pine10.jpg) | ![nokia10](../nokia10.jpg) |
