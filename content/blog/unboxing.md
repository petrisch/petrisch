+++
title = "Unbox"
author = "petrisch"
date = 2020-07-05
+++

# Pinephone Unboxing Pictures

I will not go through the "what you get" and "what that compares to others",
just a few pictures of what that little piña looks like.

![ubo1](../ubo1.jpg)
![ubo2](../ubo2.jpg)
![ubo3](../ubo3.jpg)
![ubo4](../ubo4.jpg)
![ubo5](../ubo5.jpg)
![ubo6](../ubo6.jpg)
![ubo7](../ubo7.jpg)
![ubo8](../ubo8.jpg)
