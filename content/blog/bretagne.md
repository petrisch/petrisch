+++
title = "Bretagne"
author = "petrisch"
date = 2021-07-23
+++

## Holiday Photos from July 2021

**Little Church in the Swiss Jura mountains**  
![Church in the Swiss Jura](../IMG20210709080756.jpg)

**The cathedral of Quimper, with a carousel beneath**  
![Church in Vannes](../IMG20210712173122.jpg)

**A sea gull at the beach looking for food**  
![Sea gull at the beach](../IMG20210713130734_01.jpg)

**Lighthouses on the harbour of the "île de groix"**  
![Lighthouses on the island of groix](../IMG20210713152322.jpg)

**Another sea gull enjoying the seaside, sitting on a wall**  
![Sea gull on a wall](../IMG20210713152420.jpg)

**A table in front of an old castle, perfect place for diner**  
![Table in front of a castle](../IMG20210714103242.jpg)

**A little stream flowing through a wall**  
![Stream through a wall](../IMG20210714103348.jpg)

**Hidden front door of a castle**  
![Front door of a castle](../IMG20210714104854.jpg)

**This is Rogers Home, keep out!**  
![Rogers Home, keep out!](../IMG20210714105600.jpg)

**Sculpture of a bird in a old starecase**  
![Sculpture of a bird in a old starecase](../IMG20210714110733.jpg)
