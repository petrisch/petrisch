+++
title = "Megapixels"
author = "petrisch"
date = 2020-07-05
+++

# Megapixel

Megapixels is getting better and better.
This was the current state on Mobian from 2021-05-10  

![megapixels-alps](../Alp_Photos.jpg)

With Megapixel from postmarketOS comes new live to the app. The liveview is much improved.

![megapixel](../megapixel1.jpg)


