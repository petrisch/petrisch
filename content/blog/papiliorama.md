+++
title = "Papillorama"
author = "petrisch"
date = 2021-11-17
+++

# Butterfly

Considering the fact that it takes quite some time to take a picture with the pinephone, I am pretty happy with this picture of a butterfly, taken at the papiliorama in switzerland.

![Butterfly](../schmetterling_blatt_darktable.jpg)
