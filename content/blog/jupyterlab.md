+++
title = "Experiences with Jupiter Lab"
author = "petrisch"
date = 2021-01-19
+++

# Jupyter Lab

Today I learned some lessons about data analysis. Me and my colleages from the course I took last year at school had already done a nice work about gathering train ticket selling information from various sources and putting it on a geografical map.  

That was a nice project because it was a real life thing. Unfortunately I can't show it here because it counted as an exam, and it is still being used.  

But back to today. We had a real real life problem. We have a quite new team in our departement, and for the first time, we try to analyse more about whats goin on in sales and business.  

So one of our team members tried to gather information from sources that where not reliable yet. He did an amazing job I think, by putting everithing in one Excel file. And then he struggled in two ways.  

- How can we verify that the final table is congruent to the input?
- How can we explain to others how we got there?

Thats a fundamental problem with Spreadsheet. They only show the state. How you got there and if all this makes sense or not, we can't really know for shure.  

So my suggestion was to use the jupiter lab that I knew from the course. We are still in the middle of finding out, wheter it pays of. But some things are for shure, and that makes me happier and more confident about it:

- I can rely on this thing
- I can refill it with new data without touching any of the code
- I can explain what I do while I'm doing it

I think I only came to see the true value of it right now.
