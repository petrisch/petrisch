+++
title = "Tabletennis"
author = "petrisch"
date = 2020-12-17
+++

# This section is dedicated to my favorite sport

Table tennis is my big passion.
I am long time player and coach in the swiss [MTTV](https://www.mttv.ch)

## 2022 -

Back to the roots. Trying to just relax and enjoy the sport.
My favorite blade Persson Powerplay with a Hurricane on VH
and just a normal BH rubber is enough.

## 2015 - 2022

I tried to play a attacking "modern" Defence style, since I used to be attacker for a long time, but then got kind of bored with it.

My equipment then:

 - Blade Donic Shiono Def
 - TSP SpinPips Chop in 1,5mm
 - DHS Hurricane 3 with soft sponge

With the new balls I think its harder to defend, however if you look how Hou Yingchao still can play at his age. There is still a lot of potential in that style.

The first factor to success is of course the amount of training.
Since my trainig is reduced to a minimum, there is not much progress to expect.

## 1995 - 2015

I tried to get better every day.
Visited lots of camps and played tournaments.
Went 3 times to China to see how the best are preparing.

Haven't really been professional but felt that way :-)
In my best years I used to train about 4-5 times a week.
That was progress.
