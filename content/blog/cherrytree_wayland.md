+++
title = "Cherrytree Wayland issue"
author = "petrisch"
date = 2020-07-05
+++

# Cherrytree Wayland error

The actual problem is with wayland.
Activating it on flatseal gives this error:

```
/app/lib/python2.7/site-packages/gtk-2.0/gtk/__init__.py:57: GtkWarning: could not open display
  warnings.warn(str(e), _gtk.Warning)
/app/share/cherrytree/modules/core.py:69: Warning: invalid (NULL) pointer instance
  self.window = gtk.Window()
/app/share/cherrytree/modules/core.py:69: Warning: g_signal_connect_data: assertion 'G_TYPE_CHECK_INSTANCE (instance)' failed
  self.window = gtk.Window()
/app/share/cherrytree/modules/clipboard.py:93: GtkWarning: IA__gtk_clipboard_get_for_display: assertion 'display != NULL' failed
  self.clipboard = gtk.clipboard_get()
/app/share/cherrytree/modules/core.py:114: Warning: invalid (NULL) pointer instance
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: Warning: g_signal_connect_data: assertion 'G_TYPE_CHECK_INSTANCE (instance)' failed
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: Warning: value "TRUE" of type 'gboolean' is invalid or out of range for property 'visible' of type 'gboolean'
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: GtkWarning: IA__gdk_screen_get_display: assertion 'GDK_IS_SCREEN (screen)' failed
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:114: Warning: g_object_ref: assertion 'G_IS_OBJECT (object)' failed
  vbox_main.pack_start(self.ui.get_widget("/MenuBar"), False, False)
/app/share/cherrytree/modules/core.py:119: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  self.scrolledwindow_tree = gtk.ScrolledWindow()
/app/share/cherrytree/modules/core.py:121: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  self.scrolledwindow_text = gtk.ScrolledWindow()
/app/share/cherrytree/modules/core.py:173: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  self.treeview.append_column(self.column)
/app/share/cherrytree/modules/core.py:173: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  self.treeview.append_column(self.column)
/app/share/cherrytree/modules/core.py:1307: Warning: invalid (NULL) pointer instance
  self.node_menu_tree = gtk.Menu()
/app/share/cherrytree/modules/core.py:1307: Warning: g_signal_connect_data: assertion 'G_TYPE_CHECK_INSTANCE (instance)' failed
  self.node_menu_tree = gtk.Menu()
/app/share/cherrytree/modules/core.py:1272: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1272: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1272: Warning: value "TRUE" of type 'gboolean' is invalid or out of range for property 'visible' of type 'gboolean'
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1274: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  menu_item.set_image(gtk.image_new_from_stock(attributes[0], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:1274: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  menu_item.set_image(gtk.image_new_from_stock(attributes[0], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:1274: Warning: value "TRUE" of type 'gboolean' is invalid or out of range for property 'visible' of type 'gboolean'
  menu_item.set_image(gtk.image_new_from_stock(attributes[0], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:1263: Warning: invalid (NULL) pointer instance
  curr_submenu = gtk.Menu()
/app/share/cherrytree/modules/core.py:1263: Warning: g_signal_connect_data: assertion 'G_TYPE_CHECK_INSTANCE (instance)' failed
  curr_submenu = gtk.Menu()
/app/share/cherrytree/modules/core.py:1264: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1264: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1264: Warning: value "TRUE" of type 'gboolean' is invalid or out of range for property 'visible' of type 'gboolean'
  menu_item = gtk.ImageMenuItem(attributes[1])
/app/share/cherrytree/modules/core.py:1265: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  menu_item.set_image(gtk.image_new_from_stock(attributes[2], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:1265: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  menu_item.set_image(gtk.image_new_from_stock(attributes[2], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:1265: Warning: value "TRUE" of type 'gboolean' is invalid or out of range for property 'visible' of type 'gboolean'
  menu_item.set_image(gtk.image_new_from_stock(attributes[2], gtk.ICON_SIZE_MENU))
/app/share/cherrytree/modules/core.py:212: GtkWarning: IA__gdk_pango_context_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: PangoWarning: pango_context_set_font_description: assertion 'context != NULL' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: PangoWarning: pango_context_set_base_dir: assertion 'context != NULL' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: PangoWarning: pango_context_set_language: assertion 'context != NULL' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: Warning: g_object_unref: assertion 'G_IS_OBJECT (object)' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: GtkWarning: IA__gtk_settings_get_for_screen: assertion 'GDK_IS_SCREEN (screen)' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: GtkWarning: IA__gdk_screen_get_display: assertion 'GDK_IS_SCREEN (screen)' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: GtkWarning: IA__gdk_keymap_get_for_display: assertion 'GDK_IS_DISPLAY (display)' failed
  self.scrolledwindow_text.add(self.sourceview)
/app/share/cherrytree/modules/core.py:212: Warning: g_object_get: assertion 'G_IS_OBJECT (object)' failed
  self.scrolledwindow_text.add(self.sourceview)
```



