+++
title = "Computer aided design"
author = "petrisch"
date = 2021-01-09
+++

## Computer aided design

As a original mechanical engineer I have worked with some CAD Software.

- TopCAD on old Macs (best 2D Functions ever!)
- UG and ME10 in technical school
- HiCAD long time at work
- FreeCAD in Freetime

I love FreeCAD even though I haven't been able to use it for a bigger project. But I love how many different modules there are. It can really do anything. There are bugs, yes, in a professional environement, it would have a hard time with its stressed users, but still, great peace of software.

## 2023 New Version

With the FPA in place and a new release, its even better.
Check it out:

[FreeCAD](https://www.freecad.org/)
