+++
title = "Bern 2021"
author = "petrisch"
date = 2021-09-25
+++

## The city of Bern shot with Megapixels 1.3

These pictures are not edited in any way so they reflect the actual progress.  
If haven't been very patient with some pictures, thats why they are a little blurry. Not the fault of the app. Also I'm still bad at making pictures.  

A street in the lower old town.  

![Mohnhaus](../Mohnhaus.jpg)

A sign on a bin saying it wants to get feeded in german.  

![Bitte fuettern](../fuettern.jpg)

A lions head with water coming out of his mouth acting as a fountain.  

![Brunnen](../brunnen.jpg)

A toddler scrambling for its parents.  

![Baby](../baby.jpg)

The all famous view from the rosegarden to the lower city.  

![lower bern](../lower_bern.jpg)
