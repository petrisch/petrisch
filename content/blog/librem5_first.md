+++
title = "Librem 5 camera"
author = "petrisch"
date = 2023-03-12
+++

# Librem 5 spring flowers

It is not pinephone pictures this time. Even though the pine is still active in use, it is faster to take a picture with the librem.

<!-- {{ resize_image(path=page.path ~ "evprimeli.jpg", width=100, height=50, op="fit_width") }} -->
<!-- {{ dimmable_image(src="blog/evprimeli.jpg", alt="primeli") }} -->

<!-- <img src="../evprimeli.jpg" alt="drawing" width="612"/> -->
<!-- Some purple flower. -->

<!-- <img src="../irgendwas.jpg" alt="drawing" width="612"/> -->
<!-- Some blue flower. -->

<!-- <img src="../zapfen_auf_stein.jpg" alt="drawing" width="612"/> -->
<!-- A fir cone on a stone -->

![ev. Primeli](../evprimeli.jpg)
Some purple flower

![irgendwas](../irgendwas.jpg)
Some blue flower

![Zapfen auf Stein](../zapfen_auf_stein.jpg)
A fir cone on a stone

