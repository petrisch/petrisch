+++
title = "Wodden Medal project"
author = "petrisch"
date = 2021-02-18
+++

# Wooden Medal Project

## Target

Years ago our association decided to no longer give away traditional medals for the first three of a competition. It was just not hot enough anymore. Players were throwing them in the bin.  
So for quite a time the prices have only been gift cards or the like.  

Then new dynamic came into play, and the demand especially for young players as to have something symbolic instead of something usefull.  

I was myself in Mexico at that time. That would be another topic but it motivated me to create something new, original instead of just reintroducing the traditional medals from stock.  

## Concept ideas, road to

I had time but not much to tinker with. So most of the concept happend between my eyes and the conversation with my girlfriend. Many thanks to her!  

My first design went on a beautiful banana paper.  
![Entwurf](../Medaille_1.png)

After that I had to convince my colleagues.  
With a first digital drawing and an idea about the colors, we had this.  
![VDigital painting](../Sketch_Medaille.png)


From there on, I took the road to production. So first we needed a vector variant that was more precise. With this I went to a potential producer. He had a lot of important remarcs on how this could be done and where the cost effective parts are.  
![Vector grafics](../Medaile_einzeln.png)

## Final concept

So the final concept was this.  
The medal should be something you want to collect. With the idea, that you can staple the medals together, there should be motivation to get more than just one, but 3 ore more medals.  

The association is politicaly staged in three cantons, and our original logo takes that into account. I wanted that too. So now the three cantons flags come together, one you have 3 medals together.  
![Canton concept](../Canton_concept.png)

For production we needed three layers of color. The fourth color white was gratis, because we took Ahorn wood as basic material. As a mechanical engineer, I always wanted to make something out of wood, but guess what, in my field that newer happens.  
And what could be more sustainable, quality showing, than local hard wood?  
![Color for print sieve](../Medaille_Einfach_1_schwarz.png)
![Color for print sieve](../Medaille_Einfach_1_rot.png)
![Color for print sieve](../Medaille_Einfach_1_gelb.png)


The final drawing for production was this.  
The outer lines are laser cutted. I was surprised by the sharp edges and that this fitted together very good.  
![Technische Zeichnung](../Technische_Zeichnung.png)

The printing of the label on the front side gave more work than expected. We made a first stock of non labeled medals, and a first print for the first competition. Because we didn't know how much categories this will be, that was quite a challenge.  


After some production samples and corrections there was the time for the first competition where the winners got awarded by these medals.  
![Hall presentation](../Presentation.png)

I learnt a lot in this time, and now, two years later, I already remember a lot of things I would do differently. How time flies.

I want to thank all of the people who gave tipps and tricks, the comitee for the patients, the kids for theyr smiles, the programmers of FreeCAD, HiCAD, Inkscape and Krita for theyr great work.  
And as I mentioned already my girlfriend for the patience. She is sitting next to me laughing :-)

