+++
title = "index"
description = "index"
template = "section.html"
[extra]
header = {title = "This is petrisch", img = "$BASE_URL/about/avatar.png"}
section_path = "blog/_index.md"
max_posts = 3 
social_media_card = "img/social_cards/index.jpg"
+++

This is a webpage mostly to test out new stuff.

Nothing serious just me saying hello 😉
