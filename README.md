# Origin of petrisch.codeberg.page

These are the original files to build my "test" website.
The nushell script should build the site with zola locally.
Then create a git repo and prepare for this:

[codeberg pages](https://docs.codeberg.org/codeberg-pages/)

After that in the public directory we do a:

```
git push --set-upstream origin pages -f
```

We don't track the pages repo, so this is fine.
Also we don't track the static content like pictures.
